import React from 'react'
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
} from 'react-native'
import Chart from './components/Chart'
import { colors } from './components/Chart/constants/colors'
import test from './data/test'

export default function App() {
    return (
        <SafeAreaView style={styles.container}>
            <ScrollView>
                <Chart
                    type="week"
                    // type="month"
                    items={test.week}
                    // items={test.month}
                    maxValue={1500}
                />
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex:            1,
        paddingTop:      50,
        backgroundColor: colors.background,
    },
})
