import * as PropTypes from 'prop-types'
import React, { Component } from 'react'
import {
    Text,
    View,
} from 'react-native'
import {
    Column,
    DateText,
    Footer,
    InfoRow,
    Separators,
} from './components'
import { getPercent } from './constants/measuring'
import { footerDays } from './data'
import { styles } from './styles'

class Chart extends Component
{
    /**
     * Chart columns
     *
     * @type {*[]}
     */
    columns = []

    /**
     * Percent column step
     *
     * @type {number}
     */
    percentStep = 20

    /**
     * Standard percents column values
     *
     * @type {number[]}
     */
    standardPercents = [0, 20, 40, 60, 80, 100]

    state = {
        activeColumns:  {
            week:  null,
            month: null,
        },
        columnsLayout:  {
            week:  {},
            month: {},
        },
        percentsColumn: [],
    }

    componentDidMount()
    {
        this.getPercentsColumnValues()
    }

    componentDidUpdate(prevProps)
    {
        const { type } = this.props

        if (prevProps.type !== type) {
            this.getPercentsColumnValues()
        }
    }

    /**
     * Get percents column values
     *
     * @return {undefined}
     */
    getPercentsColumnValues = () => {
        const { items, maxValue } = this.props

        let maxItemsValue = maxValue

        items.map(item => {
            if (item.value > maxItemsValue) {
                maxItemsValue = item.value
            }
        })

        // Set standard percents column
        if (maxItemsValue === maxValue) {
            this.setState({
                percentsColumn: this.standardPercents.reverse(),
            })

            return
        }

        // Set custom percents column
        const maxPercent = maxItemsValue * 100 / maxValue

        /**
         * "+ 1" for "0%" value
         *
         * @type {number}
         */
        const overPercentsCount = Math.ceil(maxPercent / this.percentStep) + 1
        const percentsColumn    = []

        for (let i = 0; i < overPercentsCount; i += 1) {
            const value = this.percentStep * (i)

            percentsColumn.push(value)
        }

        this.setState({
            percentsColumn: percentsColumn.reverse(),
        })
    }

    /**
     * Set active column
     *
     * @param {number} index
     *
     * @return {undefined}
     */
    setActiveColumn = index => {
        const { activeColumns, columnsLayout } = this.state
        const { type }                         = this.props

        this.columns[index]._children[0].measure((width, height, px, py, fx, fy) => {
            const columnLayout = {
                fx, fy,
                px, py,
                width,
                height,
            }

            this.setState({
                activeColumns: {
                    ...activeColumns,
                    [type]: index,
                },
                columnsLayout: {
                    ...columnsLayout,
                    [type]: columnLayout,
                },
            })
        })
    }

    render()
    {
        const {
                  activeColumns,
                  columnsLayout,
                  percentsColumn,
              } = this.state
        const {
                  type,
                  items,
                  maxValue,
              } = this.props

        const itemsLength = items.length

        return percentsColumn.length > 0 && (
            <View style={styles.container}>
                <View style={styles.wrapper}>
                    <InfoRow
                        columnLayout={columnsLayout[type]}
                        percent={activeColumns[type] !== null ? getPercent(items[activeColumns[type]].value, maxValue) : null}
                        ml={activeColumns[type] !== null ? items[activeColumns[type]].value : null}
                    />

                    <View style={styles.chartRow}>
                        <View style={styles.percentsContainer}>
                            {percentsColumn.map((percent, i) => (
                                <View
                                    key={i}
                                    style={[
                                        styles.percent,
                                        i === 0 ? styles.percentLast : '',
                                    ]}
                                >
                                    <Text style={styles.percentsText}>
                                        {`${percent}%`}
                                    </Text>
                                </View>
                            ))}
                        </View>

                        <View style={styles.chartPalette}>
                            {itemsLength > 0 && items.map((item, i) => {
                                return (
                                    <Column
                                        key={i}
                                        ref={r => {
                                            this.columns[i] = r
                                        }}
                                        type={type}
                                        item={item}
                                        onPress={() => this.setActiveColumn(i)}
                                        isActive={i === activeColumns[type]}
                                        itemsLength={itemsLength}
                                        maxValue={maxValue}
                                    />
                                )
                            })}

                            <Separators
                                type={type}
                                columnLayout={columnsLayout[type]}
                                itemsLength={itemsLength}
                            />
                        </View>
                    </View>

                    <View style={styles.daysWrapper}>
                        {items.map((item, i) => {
                            return (
                                <DateText
                                    key={i}
                                    index={i}
                                    text={item.date}
                                    type={type}
                                    isActive={i === activeColumns[type]}
                                    itemsLength={itemsLength}
                                    columnLayout={columnsLayout[type]}
                                />
                            )
                        })}
                    </View>
                </View>

                <Footer days={footerDays} />
            </View>
        )
    }
}

Chart.propTypes = {
    items:    PropTypes.array,
    type:     PropTypes.oneOf(['month', 'week']),
    maxValue: PropTypes.number,
}

Chart.defaultProps = {
    items:    [],
    type:     'week',
    maxValue: 1500,
}

export default Chart

