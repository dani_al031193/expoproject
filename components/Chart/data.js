export const footerDays = {
    current: 3,
    longest: 12,
}

export const chartTypes = {
    week:  'week',
    month: 'month',
}
