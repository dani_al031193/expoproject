export const colors = {
    primary:    '#54C7FC',
    background: 'rgba(73, 164, 248, 0.05)',
    dark:       '#043353',
    text:       'rgba(4,51,83, .3)',
    white:      '#ffffff',
}
