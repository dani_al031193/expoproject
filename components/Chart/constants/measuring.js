import { chartTypes } from '../data'
import { wp } from '../helpers'
import { styles } from '../styles'

/**
 * Get percent
 *
 * @param {number} value
 * @param {number} maxValue
 *
 * @return {number}
 */
export const getPercent = (value, maxValue) => {
    return Math.round(value * 100 / maxValue)
}

/**
 * Get column height on percent
 *
 * @param {number} value
 * @param {number} maxValue
 *
 * @return {number}
 */
export const getColumnHeight = (value, maxValue) => {
    const percent = getPercent(value, maxValue)

    /**
     * 5 - "100%" percents column values count without first value.
     * For example: [0 , 20, 40, 60, 80, 100].length - 1
     *
     * @type {number}
     */
    const percentHeight = (styles.percent.paddingTop + 22) * 5 / 100

    return percentHeight * percent
}

/**
 * Get column width
 *
 * @param {string} type
 *
 * @return {number}
 */
export const getColumnWidth = type => {
    if (type === chartTypes.week) {
        return wp(2.400) // 9
    } else {
        return wp(0.800) // 3
    }
}

const weekDays = [
    'SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT',
]

/**
 * Get formatted date
 *
 * @param {Date} date
 * @param {boolean} withYear
 *
 * @return {string}
 */
export const getFormattedDate = (date, withYear = false) => {
    const d = new Date(date * 1000)

    let month  = '' + (d.getMonth() + 1)
    let day    = '' + d.getDate()
    const year = d.getFullYear()

    if (month.length < 2)
        month = '0' + month
    if (day.length < 2)
        day = '0' + day

    if (withYear) {
        return `${day}.${month}\n${year} `
    }

    return `${day}.${month}`
}

/**
 * Get formatted week day
 *
 * @param {Date} date
 *
 * @return {string}
 */
export const getWeekDay = date => {
    const d = new Date(date * 1000)

    const weekDayNumber = d.getDay()

    return weekDays[weekDayNumber]
}
