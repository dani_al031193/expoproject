import { StyleSheet } from 'react-native'
import { colors } from './constants/colors'
import {
    fs,
    hp,
    wp,
} from './helpers'

export const chartWidth = wp(84.000) // 315

const styles = StyleSheet.create({
    container:              {
        paddingHorizontal: wp(4.267), // 16
    },
    wrapper:                {
        backgroundColor:   colors.white,
        borderRadius:      25,
        paddingHorizontal: wp(3.467), // 13
        paddingTop:        hp(3.202), // 26
        paddingBottom:     hp(4.433), // 36
    },
    chartRow:               {
        flexDirection: 'row',
        width:         chartWidth,
    },
    infoContainer:          {
        position:     'relative',
        marginBottom: hp(3.325), // 27
        height:       hp(4.926), // 40
    },
    infoContainerAbsolute:  {
        position: 'absolute',
    },
    infoPercentText:        {
        fontSize:   fs(4.800), // 18
        fontWeight: '600',
        color:      colors.dark,
    },
    infoMlText:             {
        fontSize: fs(3.733), // 14
        color:    colors.text,
    },
    percentsContainer:      {
        alignItems: 'center',
        width:      wp(9.867), // 37
    },
    percent:                {
        paddingTop: hp(2.956), // 24
    },
    percentLast:            {
        paddingTop: 0,
    },
    percentsText:           {
        fontSize: 14,
        color:    colors.text,
    },
    chartPalette:           {
        position:       'relative',
        flexDirection:  'row',
        flex:           1,
        justifyContent: 'space-between',
        alignItems:     'flex-end',
    },
    columnCover:            {
        justifyContent: 'center',
        alignItems:     'center',
    },
    column:                 {
        backgroundColor: colors.primary,
        borderRadius:    25,
    },
    columnActive:           {
        backgroundColor: colors.dark,
    },
    daysWrapper:            {
        flexDirection:  'row',
        paddingLeft:    wp(9.867), // 37
        width:          chartWidth,
        justifyContent: 'space-between',
        paddingTop:     hp(2.094), // 17
    },
    daysWrapperBottomSpace: {
        paddingBottom: 20,
    },
    day:                    {
        alignItems:     'center',
        justifyContent: 'center',
        width:          wp(9.600), // 36
    },
    dayText:                {
        fontSize:  14,
        color:     colors.text,
        textAlign: 'center',
    },
    dayTextActive:          {
        color: colors.dark,
    },
    separatorHorizontal:    {
        position: 'absolute',
        left:     0,
        right:    0,
    },
    lineHorizontal:         {
        position: 'absolute',
        left:     0,
        right:    0,
        height:   1,
        width:    '100%',
    },
    separatorVertical:      {
        position: 'absolute',
        top:      0,
        bottom:   0,
    },
    lineVertical:           {
        position: 'absolute',
        top:      0,
        bottom:   0,
        width:    1,
        height:   '100%',
    },
})

const footerStyles = StyleSheet.create({
    container:       {
        flexDirection:     'row',
        justifyContent:    'space-between',
        paddingTop:        hp(3.941), // 32
        paddingHorizontal: wp(4.267), // 16
    },
    block:           {
        minWidth:          wp(36.267), // 136
        paddingVertical:   hp(1.601), // 13
        paddingHorizontal: wp(4.267), // 16
        borderRadius:      25,
        backgroundColor:   colors.white,
    },
    row:             {
        flexDirection:  'row',
        justifyContent: 'space-between',
    },
    numberContainer: {},
    bigText:         {
        fontSize:   wp(5.333), // 20
        fontWeight: '700',
        color:      colors.dark,
    },
    dayText:         {
        fontSize:   wp(3.733), // 14
        fontWeight: '500',
    },
    iconContainer:   {},
    streakContainer: {
        paddingTop: hp(0.862), // 7
    },
    streakText:      {
        fontSize: wp(3.733), // 14
        color:    colors.text,
    },
})

export {
    styles,
    footerStyles,
}
