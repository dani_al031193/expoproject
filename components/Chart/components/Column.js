import * as PropTypes from 'prop-types'
import React, {
    forwardRef,
    useEffect,
    useState,
} from 'react'
import {
    Animated,
    TouchableOpacity,
    View,
} from 'react-native'
import { colors } from '../constants/colors'
import {
    getColumnHeight,
    getColumnWidth,
} from '../constants/measuring'
import { useAnimation } from '../helpers'
import { styles } from '../styles'

const Column = forwardRef(({ item, isActive, onPress, type, itemsLength, maxValue }, ref) => {
    const [isStarted, setIsStarted] = useState(false)

    useEffect(() => {
        setIsStarted(true)
    }, [isStarted])

    const animation     = useAnimation({ doAnimation: isActive, duration: 200 })
    const initAnimation = useAnimation({ doAnimation: isStarted, duration: 1000 })

    return (
        <TouchableOpacity
            onPress={onPress}
            activeOpacity={0.9}
        >
            <View
                ref={ref}
                style={[
                    styles.columnCover,
                    {
                        width: (styles.chartRow.width - styles.percentsContainer.width) / itemsLength,
                    },
                ]}
            >
                <Animated.View
                    style={[
                        styles.column,
                        {
                            backgroundColor: animation.interpolate({
                                inputRange:  [0, 1],
                                outputRange: [colors.primary, colors.dark],
                            }),
                            height:          initAnimation.interpolate({
                                inputRange:  [0, 1],
                                outputRange: [0, getColumnHeight(item.value, maxValue)],
                            }),
                            width:           getColumnWidth(type),
                        },
                    ]}

                />
            </View>
        </TouchableOpacity>
    )
})

Column.propTypes = {
    type:     PropTypes.string.isRequired,
    item:     PropTypes.object,
    isActive: PropTypes.bool,
    onPress:  PropTypes.func,
    maxValue: PropTypes.number,
}

Column.defaultProps = {
    item:     {},
    isActive: false,
    onPress:  () => null,
    maxValue: 0,
}

export { Column }
