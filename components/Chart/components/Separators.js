import * as PropTypes from 'prop-types'
import React from 'react'
import {
    ImageBackground,
    View,
} from 'react-native'
import { getColumnWidth } from '../constants/measuring'
import { chartTypes } from '../data'
import lineHorizontal from '../images/lineHorizontal.png'
import lineVertical from '../images/lineVertical.png'
import {
    chartWidth,
    styles,
} from '../styles'

const Separators = ({ columnLayout, type, itemsLength }) => {
    return Object.entries(columnLayout).length > 0 && <>
        <View style={[
            styles.separatorHorizontal,
            {
                bottom: columnLayout.py,
            },
        ]}>
            <ImageBackground
                source={lineHorizontal}
                style={styles.lineHorizontal}
            />
        </View>

        <View style={[
            styles.separatorVertical,
            {
                left: type === chartTypes.week
                          ? columnLayout.fx - styles.percentsContainer.width - (chartWidth / itemsLength / 2) - (getColumnWidth(type) / 3)
                          : columnLayout.fx - styles.percentsContainer.width - (chartWidth / itemsLength / 0.5) - (getColumnWidth(type) * 2),
            },
        ]}>
            <ImageBackground
                source={lineVertical}
                style={styles.lineVertical}
            />
        </View>
    </>
}

Separators.propTypes = {
    columnLayout: PropTypes.object,
    type:         PropTypes.string,
    itemsLength:  PropTypes.number,
}

Separators.defaultProps = {
    columnLayout: {},
    type:         '',
    itemsLength:  0,
}

export { Separators }
