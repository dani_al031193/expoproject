import * as PropTypes from 'prop-types'
import React from 'react'
import {
    Text,
    View,
} from 'react-native'
import AnimateNumber from 'react-native-animate-number'
import { styles } from '../styles'

const zeroPoint = styles.percentsContainer.width

const InfoRow = ({ columnLayout, percent, ml }) => {
    return (
        <View style={styles.chartRow}>
            <View style={styles.infoContainer}>
                <View style={[
                    styles.infoContainerAbsolute,
                    {
                        left: columnLayout.fx - zeroPoint || zeroPoint,
                    },
                ]}>
                    {percent !== null && <>
                        <Text style={styles.infoPercentText}>
                            <AnimateNumber
                                value={percent}
                                countBy={3}
                                timing="easeOut"
                                interval={1}
                            />
                            {'%'}
                        </Text>
                        <Text style={styles.infoMlText}>
                            <AnimateNumber
                                value={ml}
                                countBy={33}
                                timing="easeOut"
                                interval={1}
                            />
                            {'ML'}
                        </Text>
                    </>}
                </View>
            </View>
        </View>
    )
}

InfoRow.propTypes = {
    columnLayout: PropTypes.object,
    percent:      PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.object,
    ]),
    ml:           PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.object,
    ]),
}

InfoRow.defaultProps = {
    columnLayout: {},
    percent:      null,
    ml:           null,
}

export { InfoRow }
