import * as PropTypes from 'prop-types'
import React from 'react'
import {
    Animated,
    Text,
    View,
} from 'react-native'
import { colors } from '../constants/colors'
import {
    getFormattedDate,
    getWeekDay,
} from '../constants/measuring'
import { chartTypes } from '../data'
import {
    hp,
    useAnimation,
} from '../helpers'
import { styles } from '../styles'

const zeroPoint = styles.percentsContainer.width + (styles.day.width / 4)

const DateText = ({ isActive, text, index, itemsLength, columnLayout, type }) => {
    const animation = useAnimation({ doAnimation: isActive, duration: 200 })
    const isMonth   = type === chartTypes.month

    const firstDate = index === 0
    const lastDate  = index === itemsLength - 1

    const date = isMonth
        ? (
              firstDate && !isActive
              || lastDate && !isActive
              || isActive
          ) && getFormattedDate(text, isActive)
        : getWeekDay(text)

    return <>
        <View style={[
            styles.day,
            {
                ...(isMonth ? {
                    position: 'absolute',

                    ...(isActive && {
                        left:            columnLayout.fx - zeroPoint || zeroPoint,
                        top:             hp(1.232), // 10
                        backgroundColor: colors.white,
                    }),
                    ...(firstDate && !isActive && {
                        top:  hp(1.847), // 15
                        left: styles.percentsContainer.width,
                    }),
                    ...(lastDate && !isActive && {
                        top:   hp(1.847), // 15
                        right: 0,
                    }),
                } : {
                    width: (styles.chartRow.width - styles.percentsContainer.width) / itemsLength,
                }),
            },
        ]}>
            <Animated.Text style={[
                styles.dayText,
                {
                    color: animation.interpolate({
                        inputRange:  [0, 1],
                        outputRange: [colors.text, colors.dark],
                    }),
                },
            ]}>
                {date}
            </Animated.Text>
        </View>

        {/* Save block height with absolute positioning items hack */}
        {isMonth && (
            <View style={styles.day}>
                <Text style={styles.dayText}> </Text>
            </View>
        )}
    </>
}

DateText.propTypes = {
    isActive:     PropTypes.bool,
    text:         PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string,
    ]),
    index:        PropTypes.number,
    itemsLength:  PropTypes.number,
    columnLayout: PropTypes.object,
    type:         PropTypes.string,
}

DateText.defaultProps = {
    isActive:     false,
    text:         '',
    index:        0,
    itemsLength:  0,
    columnLayout: {},
    type:         '',
}

export { DateText }
