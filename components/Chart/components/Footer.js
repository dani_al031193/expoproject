import * as PropTypes from 'prop-types'
import React from 'react'
import {
    Text,
    View,
} from 'react-native'
import AnimateNumber from 'react-native-animate-number'
import { footerStyles as styles } from '../styles'

const Block = ({ day, type }) => {
    return (
        <View style={styles.block}>
            <View style={styles.row}>
                <View style={styles.numberContainer}>
                    <Text style={styles.bigText}>
                        <AnimateNumber
                            value={day}
                            countBy={1}
                            timing="easeOut"
                        />

                        <Text style={styles.dayText}>
                            {` days`}
                        </Text>
                    </Text>
                </View>
                <View style={styles.iconContainer}>
                    <Text style={styles.bigText}>
                        {
                            type === 'current'
                                ? '⭐️'
                                : '🔥' // <-- This is really icon
                        }
                    </Text>
                </View>
            </View>
            <View style={styles.streakContainer}>
                <Text style={styles.streakText}>
                    {
                        type === 'current'
                            ? 'Current streak'
                            : 'Longest Streak'
                    }
                </Text>
            </View>
        </View>
    )
}

Block.propTypes = {
    day:  PropTypes.number.isRequired,
    type: PropTypes.oneOf(['current', 'longest']).isRequired,
}

const Footer = ({ days }) => {
    return (
        <View style={styles.container}>
            <Block day={days.current} type="current" />
            <Block day={days.longest} type="longest" />
        </View>
    )
}

Footer.propTypes = {
    days: PropTypes.object,
}

Footer.defaultProps = {
    days: {
        current: 4,
        longest: 11,
    },
}

export { Footer }
